// Compile with `gcc -g -o detours.so -Wall -fPIC -shared -ldl preload_detours.c`

#define _GNU_SOURCE // Required for dlfcn.h to define RTLD_NEXT (see features.h)
#include <stdio.h>
#include <stdarg.h>
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>

// bin/dedicated_srv.so is what is calling open/write to /dev/tty
// open is called with O_RDWR | O_NONBLOCK

typedef int (*open_type)(const char *path, int oflag, ...);
int open(const char *path, int oflag, ...) {
    void *orig_args = __builtin_apply_args(); // Save our original calling args
    /*fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: Entered open(%s, %i, ...)\n", getpid(), path, oflag);

    if (strcmp(path, "/dev/tty") == 0) {
        fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] HIT!!!!!! RETURNING 1 for STDOUT\n", getpid());
        return -1; //stdout
    }*/

    static open_type orig_open = NULL;
    if (!orig_open) {
        orig_open = (open_type)dlsym(RTLD_NEXT, "open");
        if (orig_open) {
            fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: Found original open(): %p\n", getpid(), orig_open);
        } else {
            fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: ERROR!!!!!!!", getpid());
        }
    }

    // Call the real open with the original args we saved earlier
    void *retval = __builtin_apply((void*)orig_open, orig_args, 128); // gcc/gcc/builtins.c:1568
    __builtin_return(retval);
}

typedef int (*write_type)(int fildes, const void *buf, size_t nbyte);
ssize_t write(int fildes, const void *buf, size_t nbyte) {
    void *orig_args = __builtin_apply_args(); // Save our original calling args

    // Find what the FD points to
    char fdpathstr[1024];
    memset(fdpathstr, '\0', sizeof(fdpathstr));
    sprintf(fdpathstr, "/proc/self/fd/%i", fildes);

    struct stat fdstatbuffer;
    lstat(fdpathstr, &fdstatbuffer);
    fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: Entered write(%s [%i], %p, %zu)\n", getpid(), slinkpathstr, fildes, buf, nbyte);

    static write_type orig_write = NULL;
    if (!orig_write) {
        orig_write = (write_type)dlsym(RTLD_NEXT, "write");
        if (orig_write) {
            fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: Found original write(): %p\n", getpid(), orig_write);
        } else {
            fprintf(stderr, "~~~~~~~~~~~~~ [PID: %i] LD_PRELOAD DEBUG: ERROR!!!!!!!", getpid());
        }
    }

    // Call the real write with the original args we saved earlier
    void *retval = __builtin_apply((void*)orig_write, orig_args, 128); // gcc/gcc/builtins.c:1568
    __builtin_return(retval);
}
