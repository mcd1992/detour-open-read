
all: clean
	gcc -m32 -g -o writetty main.c
	gcc -m32 -g -o detours-lib.so -Wall -fPIC -shared -ldl preload_detours.c
	gcc -g -o detours-lib64.so -Wall -fPIC -shared -ldl preload_detours.c

clean:
	rm -f writetty detours-*.so
