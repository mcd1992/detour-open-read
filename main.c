#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stropts.h>

int main() {
    int ttyfd = open("/dev/tty", O_RDWR | O_CREAT | O_TRUNC, 0666);

    const char* teststr = "writeteststr\n";
    int teststrlen      = strlen(teststr);

    if (ttyfd) {
        printf("write returned: %i\n", write(ttyfd, teststr, teststrlen));

        //printf("ioctl returned: %i\n", ioctl(ttyfd, I_PLINK, stdout));

        printf("write returned: %i\n", write(ttyfd, teststr, teststrlen));

        printf("close returned: %i\n", close(ttyfd));
    }
}
